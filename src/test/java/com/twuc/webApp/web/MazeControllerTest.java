package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class MazeControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_handle_illera() {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/mazes/GROUND", String.class);
        assertEquals(HttpStatus.BAD_REQUEST, forEntity.getStatusCode());
        assertEquals("{\"message\":\"{message in exception}\"}",forEntity.getBody());
        assertEquals(MediaType.APPLICATION_JSON,forEntity.getHeaders().getContentType());
    }
}
