package com.twuc.webApp.handle;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandleController {
    @ExceptionHandler
    public ResponseEntity<String>getIllera(IllegalArgumentException ex){
        HttpHeaders httpHeaders=new HttpHeaders();
        httpHeaders.add("Content-Type","application/json");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .headers(httpHeaders)
                .body("{\"message\":\"{message in exception}\"}");

    }
}
